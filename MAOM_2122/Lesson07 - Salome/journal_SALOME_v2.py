#!/usr/bin/env python
import sys
import salome
salome.salome_init()
theStudy = salome.myStudy
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r"C:/Users/Andrea/Desktop/mesh_MAOM")
import math
import numpy as np

##########################################################################################
### GEOM component #######################################################################
##########################################################################################
import GEOM
from salome.geom import geomBuilder
import SALOMEDS
geompy = geomBuilder.New()
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geompy.addToStudy( O, "O" )
geompy.addToStudy( OX, "OX" )
geompy.addToStudy( OY, "OY" )
geompy.addToStudy( OZ, "OZ" )
if salome.sg.hasDesktop():
    salome.sg.updateObjBrowser()


### Read data from input file ............................................................
print('... Read airfoil data from input file');
airfoil = np.loadtxt('C:/Users/Andrea/Desktop/mesh_MAOM/airfoil/NACA65(12)10_v2.txt', skiprows=1)
#print(airfoil)
pt_list = []
for x in range(len(airfoil)):
    pto = geompy.MakeVertex(airfoil[x][0], airfoil[x][1], 0)
    pt_list.append(pto)
    #geompy.addToStudy( pt_list[x], "pt_"+str(x) )



### Generate airfoil ......................................................................
print('... Generate airfoil');
# Pressure and Suction sides .........................................
cutPnts = [90,110]
Pressure = geompy.MakeInterpol(pt_list[0:cutPnts[0]+1], False,False)
geompy.addToStudy( Pressure, "Pressure" )
LeadingE = geompy.MakeInterpol(pt_list[cutPnts[0]:cutPnts[1]+1], False,False)
geompy.addToStudy( LeadingE, "LeadingE" )
Suction = geompy.MakeInterpol(pt_list[cutPnts[1]:199], False,False)
geompy.addToStudy( Suction, "Suction" )



# Trailing Edge
te0 = geompy.MakeVertex(22.771810, 32.653716, 0)
geompy.addToStudy( te0, "te0" )
TrailingE = geompy.MakeArc(pt_list[0], te0, pt_list[198],False)
geompy.addToStudy( TrailingE, "TrailingE" )

#raise Exception('STOP DEBUG - Geom End') 

###Generate Topology ......................................................................
chord=40;

pDom_0 = geompy.MakeVertex(  -40.0, -60.0, 0.0)
geompy.addToStudy( pDom_0, "pDom_0" )
pDom_1 = geompy.MakeVertex(  0.0, -15.0, 0.0)
geompy.addToStudy( pDom_1, "pDom_1" )
pDom_2 = geompy.MakeVertex(  40.0, 40.0, 0.0)
geompy.addToStudy( pDom_2, "pDom_2" )
pDom_3 = geompy.MakeVertex(  100.0, 80.0, 0.0)
geompy.addToStudy( pDom_3, "pDom_3" )

pDom_4 = geompy.MakeVertex(  -40.0, -60.0+chord, 0.0)
geompy.addToStudy( pDom_4, "pDom_4" )
pDom_5 = geompy.MakeVertex(  0.0, -15.0+chord, 0.0)
geompy.addToStudy( pDom_5, "pDom_5" )
pDom_6 = geompy.MakeVertex(  40.0, 40.0+chord, 0.0)
geompy.addToStudy( pDom_6, "pDom_6" )
pDom_7 = geompy.MakeVertex(  100.0, 80.0+chord, 0.0)
geompy.addToStudy( pDom_7, "pDom_7" )

Ld1 = geompy.MakeLineTwoPnt(pDom_0,pDom_4)
geompy.addToStudy( Ld1, "Ld1" )
Ld2 = geompy.MakeLineTwoPnt(pDom_3,pDom_7)
geompy.addToStudy( Ld2, "Ld2" )

Ld3 = geompy.MakeInterpol([pDom_0, pDom_1, pDom_2, pDom_3], False, False)
geompy.addToStudy( Ld3, "Ld3" )
Ld4 = geompy.MakeInterpol([pDom_4, pDom_5, pDom_6, pDom_7], False, False)
geompy.addToStudy( Ld4, "Ld4" )

'''
Domain_ext = geompy.MakeFaceWires([Ld1, Ld3, Ld2, Ld4], 1)
geompy.addToStudy( Domain_ext, "Domain_ext" )
'''



Domain = geompy.MakeFaceWires([Ld1, Ld3, Ld2, Ld4, Pressure, LeadingE, Suction, TrailingE], 1)
geompy.addToStudy( Domain, "Domain" )


### Create groups for mesh ...............................................................
print('... Create groups for mesh');
cmpFoil = geompy.MakeCompound([Suction,Pressure])
gro_Foil = geompy.GetInPlace(Domain, cmpFoil)
geompy.addToStudyInFather( Domain, gro_Foil, "gro_Foil" )
cmpSuc = geompy.MakeCompound([Suction])
gro_Suc = geompy.GetInPlace(Domain, cmpSuc)
geompy.addToStudyInFather( Domain, gro_Suc, "gro_Suc" )
cmpPre = geompy.MakeCompound([Pressure])
gro_Pre = geompy.GetInPlace(Domain, cmpPre)
geompy.addToStudyInFather( Domain, gro_Pre, "gro_Pre" )
cmpLe = geompy.MakeCompound([LeadingE])
gro_Le = geompy.GetInPlace(Domain, cmpLe)
geompy.addToStudyInFather( Domain, gro_Le, "gro_Le" )
cmpTe = geompy.MakeCompound([TrailingE])
gro_Te = geompy.GetInPlace(Domain, cmpTe)
geompy.addToStudyInFather( Domain, gro_Te, "gro_Te" )
cmpInout = geompy.MakeCompound([Ld1, Ld2])
gro_Inout = geompy.GetInPlace(Domain, cmpInout)
geompy.addToStudyInFather( Domain, gro_Inout, "gro_Inout" )
cmpPer = geompy.MakeCompound([Ld3, Ld4])
gro_Per = geompy.GetInPlace(Domain, cmpPer)
geompy.addToStudyInFather( Domain, gro_Per, "gro_Per" )
cmpFace = geompy.MakeCompound([Domain])
gro_Face = geompy.GetInPlace(Domain, cmpFace)
geompy.addToStudyInFather( Domain, gro_Face, "gro_Face" )


#raise Exception('STOP DEBUG - Geom End') 
##########################################################################################
### SMESH component ######################################################################
##########################################################################################
import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

print('... Generate mesh');
Mesh_1 = smesh.Mesh(Domain)
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')

# Global Algorithms ......................................................................
Regular_1D = Mesh_1.Segment()
Local_Length_1 = Regular_1D.LocalLength(1,None,1e-07)
NETGEN_2D = Mesh_1.Triangle(algo=smeshBuilder.NETGEN_2D)
NETGEN_2D_Parameters_1_1 = NETGEN_2D.Parameters()
NETGEN_2D_Parameters_1_1.SetMaxSize( 1.5 )
NETGEN_2D_Parameters_1_1.SetMinSize( 0.05 )
NETGEN_2D_Parameters_1_1.SetOptimize( 1 )
NETGEN_2D_Parameters_1_1.SetFineness( 3 )
#NETGEN_2D_Parameters_1_1.SetChordalError( -1 )
#NETGEN_2D_Parameters_1_1.SetChordalErrorEnabled( 0 )
#NETGEN_2D_Parameters_1_1.SetUseSurfaceCurvature( 1 )
#NETGEN_2D_Parameters_1_1.SetUseDelauney( 0 )
#NETGEN_2D_Parameters_1_1.SetQuadAllowed( 0 )
#NETGEN_2D_Parameters_1_1.SetWorstElemMeasure( 0 )
#NETGEN_2D_Parameters_1_1.SetCheckChartBoundary( 104 )

## Set names of Mesh objects
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(NETGEN_2D.GetAlgorithm(), 'NETGEN 2D')
smesh.SetName(NETGEN_2D_Parameters_1_1, 'NETGEN 2D Parameters_1')
smesh.SetName(Local_Length_1, 'Local Length_1')



# SubMeshes ............................................................................
# SubMesh_1 - Foil
'''
Regular_1D_1 = Mesh_1.Segment(geom=gro_Foil)
Foil_Number_of_Segments = Regular_1D_1.NumberOfSegments(300,None,[])
Foil_Number_of_Segments.SetConversionMode( 1 )
Foil_Number_of_Segments.SetTableFunction( [  0.00,1.00,   0.02,1.00,   0.10,0.50,   0.40,0.15,    0.60,0.15,   0.90,0.50,   0.98,1.00,   1.00,1.00] )
#Foil_Number_of_Segments.SetExpressionFunction( '1*(t-0.5)^2+0*(t-0.5)+0.05' )
smesh.SetName(Foil_Number_of_Segments, "Foil_Number_of_Segments")
'''

# SubMesh_2 - Foil_Te
Regular_1D_2 = Mesh_1.Segment(geom=gro_Te)
Te_Number_of_Segments = Regular_1D_2.NumberOfSegments(6)
smesh.SetName(Te_Number_of_Segments, "Te_Number_of_Segments")

# SubMesh_5 - Foil_Le
Regular_1D_5 = Mesh_1.Segment(geom=gro_Le)
Le_Number_of_Segments = Regular_1D_5.NumberOfSegments(60)
Le_Number_of_Segments.SetTableFunction( [  0.00,0.20,   0.2,1.00,   0.40,1.0,   1.00,0.20] )
smesh.SetName(Le_Number_of_Segments, "Le_Number_of_Segments")

# SubMesh_3 - InOut
Regular_1D_3 = Mesh_1.Segment(geom=gro_Inout)
Inout_Number_of_Segments = Regular_1D_3.NumberOfSegments(20)
smesh.SetName(Inout_Number_of_Segments, "Inout_Number_of_Segments")

# SubMesh_4 - Periodic
Regular_1D_4 = Mesh_1.Segment(geom=gro_Per)
Inout_Number_of_Segments = Regular_1D_4.NumberOfSegments(150,None,[])
Inout_Number_of_Segments.SetExpressionFunction( '-2*(t-0.5)^2+0*(t-0.5)+1' )
smesh.SetName(Inout_Number_of_Segments, "Inout_Number_of_Segments")

# SubMesh_6 - Foil_Pre
Regular_1D_6 = Mesh_1.Segment(geom=gro_Pre)
Pre_Number_of_Segments = Regular_1D_6.NumberOfSegments(180,None,[])
Pre_Number_of_Segments.SetTableFunction( [  0.00,1.00,     0.30,0.10,   0.70,0.10,    1.00,0.30] )
smesh.SetName(Pre_Number_of_Segments, "Pre_Number_of_Segments")

# SubMesh_7 - Foil_Suc
Regular_1D_7 = Mesh_1.Segment(geom=gro_Suc)
Suc_Number_of_Segments = Regular_1D_7.NumberOfSegments(180,None,[])
Suc_Number_of_Segments.SetTableFunction( [  0.00,0.30,     0.30,0.10,   0.70,0.10,    1.00,1.00] )
smesh.SetName(Suc_Number_of_Segments, "Suc_Number_of_Segments")




#raise Exception('STOP DEBUG - Geom End') 
# Boundary Layer .......................................................................
# Get edges ID for boundary layer
BL_edge=[]

edge_te = geompy.GetEdge(Domain, pt_list[0], pt_list[198])  # 100:200
edge_te.GetSubShapeIndices()
BL_edge.append(edge_te.GetSubShapeIndices()[0])

edge_ss = geompy.GetEdge(Domain, pt_list[0], pt_list[cutPnts[0]])
edge_ss.GetSubShapeIndices()
BL_edge.append(edge_ss.GetSubShapeIndices()[0])

edge_le = geompy.GetEdge(Domain, pt_list[cutPnts[0]], pt_list[cutPnts[1]])
edge_le.GetSubShapeIndices()
BL_edge.append(edge_le.GetSubShapeIndices()[0])

edge_ps = geompy.GetEdge(Domain, pt_list[cutPnts[1]], pt_list[198])
edge_ps.GetSubShapeIndices()
BL_edge.append(edge_ps.GetSubShapeIndices()[0])

# Generate BL .............................................
Viscous_Layers_2D_1 = NETGEN_2D.ViscousLayers2D(0.25,8,1.15)
#Viscous_Layers_2D_1.SetEdges( [12, 15, 17], 0 )   
Viscous_Layers_2D_1.SetEdges( BL_edge, 0 )  

#raise Exception('STOP DEBUG - Geom End') 


### Create groups for patches ............................................................
groPat_PS = geompy.GetInPlace(Domain, Pressure)
Blade_PS = Mesh_1.GroupOnGeom(groPat_PS,'groPat_PS',SMESH.EDGE)
Blade_PS.SetName('Blade_PS')

groPat_SS = geompy.GetInPlace(Domain, Suction)
Blade_SS = Mesh_1.GroupOnGeom(groPat_SS,'groPat_SS',SMESH.EDGE)
Blade_SS.SetName('Blade_SS')

groPat_TE = geompy.GetInPlace(Domain, TrailingE)
Blade_TE = Mesh_1.GroupOnGeom(groPat_TE,'groPat_TE',SMESH.EDGE)
Blade_TE.SetName('Blade_TE')

groPat_Inlet = geompy.GetInPlace(Domain, Ld1)
Inlet = Mesh_1.GroupOnGeom(groPat_Inlet,'groPat_Inlet',SMESH.EDGE)
Inlet.SetName('Inlet')

groPat_Outlet = geompy.GetInPlace(Domain, Ld2)
Outlet = Mesh_1.GroupOnGeom(groPat_Outlet,'groPat_Outlet',SMESH.EDGE)
Outlet.SetName('Outlet')

groPat_Per1 = geompy.GetInPlace(Domain, Ld3)
Side1 = Mesh_1.GroupOnGeom(groPat_Per1,'groPat_Per1',SMESH.EDGE)
Side1.SetName('Side1')

groPat_Per2 = geompy.GetInPlace(Domain, Ld4)
Side2 = Mesh_1.GroupOnGeom(groPat_Per2,'groPat_Per2',SMESH.EDGE)
Side2.SetName('Side2')

#gro_Face = geompy.GetInPlace(Domain, Domain)
FluidDomain = Mesh_1.GroupOnGeom(gro_Face,'gro_Face',SMESH.FACE)
FluidDomain.SetName('FluidDomain')

isDone = Mesh_1.Compute()

print('... Save mesh');
try:
  Mesh_1.ExportUNV( r"C:/Users/Andrea/Desktop/mesh_MAOM/sim/Mesh2D.unv" )
  pass
except:
  print("ExportUNV() failed. Invalid file name?")

print('... Mesh Generation terminated');
#raise Exception('STOP DEBUG - Geom End') 
exit()





