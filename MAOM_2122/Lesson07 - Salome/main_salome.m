clear all
close all
clc
opt.plot=0;

%% import the airfoils
% A = importdata('airfoil/NACA65(12)10.txt',' ',2);
% airfoil = A.data;
% B = load('airfoil/pressure1.mat');
% pressure = B.curveFinal;
% C = load('airfoil/suction1.mat');
% suction = C.curveFinal;

fid = fopen('airfoil/NACA65(12)10.txt','r');
fgetl(fid);
A = fscanf(fid,' %f %f %f',[3 Inf])';
fclose(fid);

n=100;
A_pre = A(1:n,:);
A_suc = A(n+1:end,:);

A_suc=flipud(A_suc);

fid = fopen('airfoil/NACA65(12)10_v2.txt','w');
fprintf(fid,'NACA65(12)10_v2\n');
fprintf(fid,' %f %f %f\n',A_suc');
fprintf(fid,' %f %f %f\n',A_pre(2:end,:)');
fclose(fid);

%% plot airfoils
if opt.plot==1
    figure(1)
    hold on
    plot(A_pre(:,1),A_pre(:,2),'r-*');
    plot(A_suc(:,1),A_suc(:,2),'b-*');
    
    axis equal
    grid on
    xlabel('x [-]');
    ylabel('y [-]');
    title('Naca651210');
    legend('PressureSide','SuctionSide','Location','NorthWest');
    
    % xlim([0 1])
    % ylim([-0.2 0.2])
    % print -depsc grafico.eps
    % print -dpng grafico.png
end

