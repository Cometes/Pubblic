function dsq = distanceFunction(decvar,x_coo,outnum,profile)
% decvar contains [y2 y3 y4 y5]    x_coo contains [x2 x3 x4 x5]
opt.plot=1;  % switch to 0 to supress the plot

bez = [x_coo' decvar'];

% define all control points
P1 = profile(1,:);
P6 = [profile(end,1)-0.005, profile(end,2)];
P7 = profile(end,:);

% join the control points in a unique matrix
pc = [P1; bez; P6; P7];

%create curve with bezier parametrization - otunum=number of points of the curve
curve = bezier(pc,outnum);

%interpolate the bezier curve on the sample of points
% origInt are the y-coordinates of the profile, evaluated in the x-coordinates of Bezier curve
origInt = interp1(profile(:,1),profile(:,2),curve(:,1));

% optional plot
if opt.plot==1
    %figure(3)
    plot(curve(:,1),curve(:,2));
    hold on;
    plot(profile(:,1),profile(:,2),'-r');
    plot(pc(:,1),pc(:,2),'-xg');
    axis equal
    axis([0 1 -0.3 0.4])
    hold off;
    drawnow;
end

% Fitness: calculates the maximum distance between the curves
dsq = max((curve(:,2)-origInt).^2);
%fprintf('%5.3f\n',dsq);

% to report in the command window the generation and individual number
% (change Ind ==100 if the nInd number is changin in main
global Gen Ind
Ind = Ind+1;
fprintf('%s %d %s %d %s %5.3f\n','Gen: ',Gen,'Ind:',Ind, 'Dsq:',dsq);
if Ind == 100
    Gen = Gen +1;
    Ind = 0;
end

% To save all the evaluatd individual and their fitness in a global variable
global Sol
rowN= (Gen)*100+(Ind);
Sol(rowN,:)=[decvar x_coo dsq];

end

