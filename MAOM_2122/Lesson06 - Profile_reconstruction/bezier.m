function points = bezier(pc,outnum)
%A: curva sottesa da bezier
n = size(pc,1)-1;
t = linspace(0,1,outnum);
ii = (0:n);
C = factorial(n)./(factorial(ii).*factorial(n-ii));
points = zeros(2,outnum);
for k=1:outnum
    points(:,k) = C.*t(k).^ii.*(1-t(k)).^(n-ii)*pc;
end
points = points';

end

