clear;
close all;
clc;

% INFO: modify the folder and profile name
folder = 'baseline/';

% considered profile and point of division
% pro='NACA65-210';    
% n=26;
pro='S813';
n=33;
% pro='NACA651210';
% n=17;

%% read the data
fid = fopen([folder,strcat(pro,'.txt')],'r');
fgetl(fid);
profile = fscanf(fid,'%f %f %f',[2 Inf])';
fclose(fid);

corda=profile(end,1);

%% plot imported profile (splitting suction and presure sides)
suction=profile(1:n,:);
suction=flipud(suction);
pressure=profile(n:end,:);
plot(pressure(:,1),pressure(:,2),'-r');
hold on
plot(suction(:,1),suction(:,2),'-b');
axis equal

%% functions to interpolate a value
% ys=interp1(suction(:,1), suction(:,2),0.995);
% yp=interp1(pressure(:,1), pressure(:,2),0.995);

%% side to consider
side=suction;

%% Optimization parameters
outnum = numel(pressure(:,1));
nInd = 100;
nGen = 20;

nVar=4;
discret=corda/(nVar);
x_coo = 0:discret:1-discret;

% Bound constraints definition
%suction side
lb = [0 -0.1 -0.1 0];
ub = [0.2 0.3 0.3 0.2];
%pressure side
% lb = ...
% ub = ...


%% initialize the population
initPop = zeros(nInd,nVar);
for k=1:nInd
    initPop(k,:) = rand(1,(nVar)).*((ub-lb))+lb;
end

%% optimization
global Gen Ind Sol
Sol = [];
Gen = 0;
Ind = 0;
options = gaoptimset('InitialPopulation',initPop,'Generations',nGen,'PopulationSize',nInd,'TolFun',1e-20);
pcS = ga(@(decvar) distanceFunction(decvar,x_coo,outnum,side),nVar,[],[],[],[],lb,ub,[],options);
pcS = [x_coo pcS];
pcS = reshape(pcS,[nVar 2]);

pcS_total =[side(1,:);     pcS;    side(end,:)-[0.015,0];    side(end,:)];

%% plot the final solution
% to verify
curveFinal = bezier(pcS_total,100);
plot(curveFinal(:,1),curveFinal(:,2));
hold on;
plot(side(:,1),side(:,2),'-r');
plot(pcS_total(:,1),pcS_total(:,2),'-xg');
axis equal
%axis([0 1 -0.3 0.4])
hold off;

%% save the data in a mat file
% save suction.mat curveFinal
% save pressure.mat curveFinal

