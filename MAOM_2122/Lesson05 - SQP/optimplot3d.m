function stop = optimplot3d(x,optimValues,~,~)

% global iter;
% iter = [iter;x' optimValues.fval];

stop = false;
figure(1);
plot3(x(1),x(2),optimValues.fval,'-xr','MarkerSize',12);
% plot3(iter(:,1),iter(:,2),iter(:,3),'-xr','MarkerSize',12);

end