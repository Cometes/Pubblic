
clear all;
close all;
clc;

% global iter;
% iter = zeros(0,3);

% Mappa punti e plot
x = (-3:0.1:3);
y = (-3:0.1:3);
val = zeros(numel(y),numel(x));
for ii=1:numel(x)
    for jj=1:numel(y)
        val(jj,ii) = rosenboth([x(ii),y(jj)]);
    end
end
surf(x,y,val); hold on;
plot3(x(x==1),y(y==1),val(y==1,x==1)+10,'-og','MarkerSize',12);
view([0 0 1]);

% Ottimizzatore
lb = [-3;-3];
ub = [3;3];
x0 = [-3;-3];
opts = optimset('Algorithm', 'sqp','OutputFcn',@optimplot3d);
[xf,fval] = fmincon(@(x) rosenboth(x),x0,[],[],[],[],lb,ub,[],opts);

plot3(xf(1),xf(2),fval,'wo','MarkerSize',15);