clear
close all
clc

%% plot in MATLAB ---------------------------------------------------------

x = 0:0.2:2*pi;
y=sin(x);
z=cos(x);
plot(x,y,'r-*');
hold on;
plot(x,z,'b-*');
axis equal
grid on
xlabel('x [rad]');
ylabel('y [-]');
title('MyPlot');
legend('sin of x','cos of x','Location','SouthWest')
print -depsc grafico.eps
% print -dpng grafico.png
% print -djpeg grafico.jpeg

%% Use subplots -----------------------------------------------------------
clear; close all; clc

x = 0:0.2:2*pi;
y=sin(x);
z=cos(x);
w=tan(x);
t=cot(x);

subplot(2,2,1)
plot(x,y,'r-*');
axis equal
xlabel('x [rad]');
ylabel('y [-]');
title('Sin');

subplot(2,2,2)
plot(x,z,'b-*');
axis equal
xlabel('x [rad]');
ylabel('z [-]');
title('Cos');

subplot(2,2,3)
plot(x,w,'k-*');
ylim([-5 5]);
xlabel('x [rad]');
ylabel('w [-]');
title('Tan');

subplot(2,2,4)
plot(x,t,'g-*');
ylim([-5 5]);
xlabel('x [rad]');
ylabel('t [-]');
title('Cot');

%% 3D plots -----------------------------------------------------------
clear; close all; clc;
punti = [0,0,0; ...
         1,1,0; ...
         2,2,1; ...
         2,1,2; ...
         5,3,3];
plot3(punti(:,1),punti(:,2),punti(:,3),'r*-');
axis equal
xlabel('x [-]');
ylabel('y [-]');
zlabel('z [-]');
title('Plot3D');


%% Surface plots -----------------------------------------------------------
clear; close all; clc;
% plottare la funzione z = x(1-x)y(1-y)
n=5; m=5;
x=linspace(0,1,n);
y=linspace(0,1,n);
[X,Y] = meshgrid(x,y);
Z = X.*(1-X).*Y.*(1-Y);
surf(X,Y,Z);



