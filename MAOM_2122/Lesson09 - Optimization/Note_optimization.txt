### Main Function ####################################################################
1) initialize the variables


2) run the gamultiobj
options=gaoptimset('PopulationSize',nInd,'InitialPopulation',initPop,'Generations',nGen);
[x,fval,exitflag,output,POPULATION,SCORE]=gamultiobj(@(decvar) fitnessFunction(decvar,x_coo,outnum),nVar,[],[],[],[],lb,ub,options);





### Fiteness Function ####################################################################
1) Reconstruct the airfoil from the decvar vector



2) Export the airfoil points for salome in txt file



3) Write Salome Journal file using the parametric variables



4) Run Salome

timeMesh=0;
while ~exist([path_cwd,'\Mesh2D.unv'],'file')
    disp('Salome is running...');
    pause(5);
    timeMesh=timeMesh+5;
    if timeMesh>=60
        disp('Salome is taking too long, terminated.');
        break;
    end
end

% Kill Salome and return to working directory
!kill_salome.bat
system(['taskkill /IM "omniNames.exe" /F']);
cd(path_cwd);


5) Write Fluent Journal file 



6) Run Fluent

system(['"C:\Program Files\ANSYS Inc\v161\fluent\ntbin\win64\fluent.exe" 2ddp -g -t4 -i Journal_Fluent.jou']);
% ! "C:\Program Files\ANSYS Inc\v161\fluent\ntbin\win64\fluent.exe" 2ddp -i Journal_Fluent.jou
while exist('velocity-angle.srp','file')~=2
    disp('Fluent is running...');
    pause(5)
end
pause(2)



7) Read the output of Fluent



8) Calculate the fitness



NOTE:
- Always avoid spaces in name of folders/files
- Implement some controls for Salome/Fluent crash --> Assign high fitness.
- Always check your results.. are the Cp, Mis, Pressure ranges correct? Are they the expected ranges?
- Normalize your plots in the space of solutions --> it will be easier to check improvements 
- Check your results after some generations. Are there weird results? If the improvment of some fitness is +50% respect the baseline, maybe somethin could be wrong.. Check the mesh and the fluent results.



